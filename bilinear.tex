\section{Bilinear forms}

\newcommand{\dual}[1]{{#1}^{\scaleobj{0.8}{\vee}}}
\newcommand{\orth}[1]{{#1}^{\perp}}


We will study bilinear forms on a finite dimensional vector space $V$ over a field $K$, i.e. linear maps $\map{\omega}{V}{\dual{V}}$.

In particular we are interested in the notion of orthogonality with respect to the form. We make the following definitions:

\begin{defn}[Orthogonal Complement]\label{defn:complement}
  For $U \inj{i} V$ a subspace, define its orthogonal complement as $\orth{U} \coloneqq \ker{ (V \ar{\omega} \dual{V} \ar{\dual{i}} \dual{U} ) }$.
\end{defn}

\begin{defn}[Orthogonality]\label{defn:orthogonality}
  For $a,b \in V$, define $a \perp b \defiff \omega(a)(b) = 0$.
\end{defn}

Note that the condition of orthogonality from definition \ref{defn:orthogonality} admits some trivial reformulations.

\begin{prop*}
  For $a,b \in V$ the following are equivalent:
  \begin{itemize}
    \item $a \perp b$
    \item $a \in \orth{\hull{b}}$
    \item $\hull{a} \subset \orth{\hull{b}}$
  \end{itemize}
\end{prop*}

Next we give the usual definition of nondegeneracy.

\begin{defn}
  A form $\omega$ is called nondegenerate if it yields an isomorphism from \mbox{$V$ to $\dual{V}$}.
  Further we will call it weakly nondegenerate, if said map is merely known to be injective (classically this is equivalent obviously).
\end{defn}

\subsection{General Properties}

Now let us note some general properties of orthogonality:

\begin{prop*}
  If $A \subset B$ in $V$ then $\orth{A} \supset \orth{B}$.
\end{prop*}

\begin{lem}[Characterisations of Symmetry]\label{lem:characterisations of symmetry}
  The following are equivalent:
  \begin{itemize}
    \item The kernel of $\omega$ as a morphism $V \otimes V \to K$ is invariant under the braiding.
    \item $\perp$ is symmetric.
    \item For all subspaces $U \hookrightarrow V$ we have $U \subset \orth{\orth{U}}$.
    \item For all one dimensional subspaces $L \hookrightarrow V$ we have $L \subset \orth{\orth{L}}$.
  \end{itemize}
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:characterisations of symmetry}]
  The first two conditions are trivially equivalent.
  Also if $\perp$ is symmetric we have for all $u \in U$ and for all $y \in \orth{U}$:
  \begin{align*}
    & \forall u' \in U.~~ \omega(y)(u') = 0 \\
    & \text{in particular } \omega(y)(u) = 0 \\
    & \text{and by symmetry } \omega(u)(y) = 0 \\
  \end{align*}
  So we conclude $u \in \orth{\orth{U}}$ for all $u \in U$, i.e. $U \subset \orth{\orth{U}}$.
  If this property holds for all subspaces, then it certainly holds for the one dimensional subspaces, and in this situation we see that $\perp$ is symmetric:
  For $a,b \in V$ with $a \perp b$, i.e. $\hull{a} \subset \orth{\hull{b}}$ taking the orthogonal complement, we find \mbox{$\hull{b} \subset \orth{\orth{\hull{b}}} \subset \orth{\hull{a}}$}, i.e. $b \perp a$.
\end{proof}

\begin{lem}[Dimension Formula]\label{lem:dimension formula}
  For all subspaces $U \hookrightarrow V$ we have
  \begin{align*}
    \dim{U} + \dim{\orth{U}} \geq{\dim{V}}
  \end{align*}
  with equality holding (for all subspaces) iff $\omega$ is surjective (i.e. an isomorphism classically).
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:dimension formula}]
  \begin{align*}
    \dim{V} &= \dim{\ker{ (V \ar{\omega} \dual{V} \ar{\dual{i}} \dual{U}) }} + \dim{\img{ (V \ar{\omega} \dual{V} \ar{\dual{i}} \dual{U}) }} \\
    &= \dim{\orth{U}} + \dim{\img{ (V \ar{\omega} \dual{V} \proj{\dual{i}} \dual{U}) }} \\
    &\leq \dim{\orth{U}} + \dim{\dual{U}} = \dim{\orth{U}} + \dim{U}
  \end{align*}
  Obviously equality holds iff $\omega$ is surjective.
\end{proof}


\begin{lem}[Characterisations of Splitting]\label{lem:characterisations of splitting}
  Given a subspace $U \inj{i} V$ we consider the induced map $U \oplus \orth{U} \ar{s} V$. The following are equivalent:
  \begin{itemize}
    \item $s$ is injective
    \item $U \cap \orth{U} = 0$
    \item $\map{i^{*} \omega}{U}{\dual{U}}$, given by $U \inj{i} V \ar{\omega} \dual{V} \proj{\dual{i}} \dual{U}$, is injective.
  \end{itemize}
  Further given an inverse of $i^{*} \omega$, we may construct an inverse of $s$. In particular if $i^{*} \omega$ is an isomorphism, then so is $s$. The converse also holds by $\dim{U} = \dim{\dual{U}}$.
\end{lem}

Remark: To actually write down an inverse of $i^{*} \omega$ given an inverse of $s$, we likely need some additional structure. \todo{What exactly?}

\begin{proof}[Proof of Lemma \ref{lem:characterisations of splitting}]
  First note that $U \cap \orth{U}$ is precisely the kernel of $i^{*} \omega$. Further it can be identified with the kernel of $s$ via exactness of \mbox{$U \cap \orth{U} \to U \oplus \orth{U} \ar{s} V$}. This proves the first claim.
  Now if $\pullback{i}\omega$ is an iso, then we obtain a projection $V \ar{p} U$ as the composition \mbox{$V \ar{\omega} \dual{V} \ar{\dual{i}} \dual{U} \iso{\inverse{(\pullback{i}\omega)}} U$}, with $p \circ i \circ p = p$. As $\ker{p} = \ker{(\dual{i} \circ \omega)} = \orth{U}$, this then yields an inverse of $s$, given by
  \begin{align*}
    V &\to U \oplus \ker{p} \\
    v &\mapsto p(v) \oplus (v - p(v))
  \end{align*}
  % Conversely if $s$ is an iso we find that $i^{*} \omega$ ..
\end{proof}

\subsection{Forms with symmetric kernel}

Motivated by Lemma \ref{lem:characterisations of symmetry}, we now turn our attention to forms with symmetric kernel. Note that both symmetric, as well as antisymmetric forms fall in this class. So in this chapter let $\omega$ have symmetric kernel.\\

First we state a simple lemma which characterizes the kernel of such forms.

\begin{lem}[Symmetric Kernel Composition]\label{lem:symmetric kernel composition}
  Let $\omega$ have symmetric kernel, and let the characteristic of the field not be 2. Decompose the form as $\omega = \mathfrak{a} + \mathfrak{s}$ into a symmetric part $\mathfrak{s}$ and an antisymmetric part $\mathfrak{a}$.
  Then we claim that $\ker{\omega} = \ker{\mathfrak{s}} \cap \ker{\mathfrak{a}}$, where all kernels are understood to be taken of the maps $V \otimes V \to K$.
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:symmetric kernel composition}]
  Let $x \otimes y \in \ker{\omega} \subset V \otimes V$ be given. Then by symmetry $y \otimes x$ is killed by $\omega$ as well and we find:
  \begin{align*}
    &\mathfrak{a}(x)(y) + \mathfrak{s}(x)(y) = 0 \text{ and } \mathfrak{a}(y)(x) + \mathfrak{s}(y)(x) = 0 \\
    \text{so }& \mathfrak{a}(x)(y) + \mathfrak{s}(x)(y) = 0 \text{ and } - \mathfrak{a}(x)(y) + \mathfrak{s}(x)(y) = 0
  \end{align*}
  Taking sums and differences, we see that both $\mathfrak{a}(x)(y) = 0$ as well as $\mathfrak{s}(x)(y) = 0$.
  The converse is trivial.
\end{proof}

We have seen that using symmetry, for any subspace $U$ we find $U \subset \orth{\orth{U}}$. The question of when we obtain equality, is answered by the following lemma:

\begin{lem}[Double Orthogonal Complement]\label{lem:double orthogonal complement}
  The following are equivalent:
  \begin{itemize}
    \item $V \ar{\omega} \dual{V}$ is an weakly nondegenerate, i.e. $\orth{V} = 0$
    \item For all $U \inj{i} V$ we have $\orth{\orth{U}} = U$.
  \end{itemize}
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:double orthogonal complement}]
  If we have $U = \orth{\orth{U}}$ for all $U \hookrightarrow V$, then in particular \mbox{$0 = \orth{(\orth{0})} = \orth{V}$}. Using the dimension formula twice (Lemma \ref{lem:dimension formula}) we obtain the converse via $\dim{U} = \dim{\orth{\orth{U}}}$.
\end{proof}

So forms with symmetric kernel, which induce an isomorphism $V \iso{} \dual{V}$ give the orthogonal complement this idempotent structure. This is the situation as usually encountered in geometry, where we consider (pseudo-)riemannian metrics or symplectic forms. But metrics actually obey a far stronger nondegeneracy condition, while the antisymmetry of symplectic forms demands some form of degenerate behaviour on subspaces. This will be outlined in the following section.

\subsection{Nondegeneracy}

\begin{defn}[Nowhere Degenerate Forms]\label{defn:nowhere degenerate forms}
  We will call a form $V \ar{\omega} \dual{V}$ nowhere degenerate, if it induces an isomorphism when restricted to any subspace of $V$, i.e. $\map{\pullback{i}\omega}{U}{\dual{U}}$ is an isomorphism for all $U \inj{i} V$. We will call it weakly nowhere degenerate, if it is injective when restricted to any subspace (this is classically equivalent obviously).
\end{defn}

Armed with this definition, we may prove the following lemma:

\begin{lem}[Characterisation of Nowhere Degeneracy]\label{lem:characterisation of nowhere degeneracy} The following are equivalent:
  \begin{itemize}
    \item The symmetric part of $\omega$ is nondegenerate, meaning that for all $v \in V$ \mbox{$\omega(v)(v) = 0 \implies v = 0$}.
    \item $\omega$ is weakly nowhere degenerate, i.e. injective on every subspace.
    \item $\omega$ is weakly nowhere degenerate on one dimensional subspaces.
  \end{itemize}
\end{lem}

Notice that this is fulfilled for, say metrics but not for e.g. pseudometrics or symplectic forms. The latter actually obey the exact opposite of the condition, in that the symmetric part vanishes completely.

\begin{proof}[Proof of Lemma \ref{lem:characterisation of nowhere degeneracy}]
  Say the form $\omega$ obeys the condition on the symmetric part, then take any subspace $U \inj{i} V$ and any $u \in U \cap \orth{U} = \ker{\pullback{i}\omega}$. As $u \in \orth{U}$, $\omega(u)(u') = 0$ for all $u' \in U$, in particular $\omega(u)(u) = 0$, so $u = 0$. Conversely if the form is nowhere degenerate, then in particuar on all one dimensional subspaces.
  Now given $v \in V$ with $\omega(v)(v) = 0$ we see that $v \in \hull{v} \cap \orth{\hull{v}} = 0$, i.e. v = 0.
\end{proof}

Everywhere weakly nondegenerate forms with symmetric kernel are precisely those for which the Gram-Schmidt orthogonalisation procedure can be applied, as captured by the following lemma:

\begin{lem}[Gram-Schmidt]\label{lem:gram-schmidt}
  Let $\omega$ be weakly nondegenerate everywhere with symmetric kernel, then given linearily independent $v_1 \dots v_n \in V$, we may construct linearily independent, orthogonal vectors $e_1, \dots e_n \in V$ inducing the same element in the exterior algebra of $V$ (in particular spanning the same subspace) as follows:
  \begin{align*}
    e_i \coloneqq v_i - \sum_{k < i} \frac{\omega(v_i)(e_k)}{\omega(e_k)(e_k)} \cdot e_k
\end{align*}
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:gram-schmidt}]
  Inductively we see that for all $i$:
  \begin{enumerate}
    \item $\omega(e_i)(e_j) = 0$ for all $j < i$. This is by construction.
    \item $e_1 \wedge \dots \wedge e_i = v_1 \wedge \dots \wedge v_i \in \bigwedge^{i}{V} $, so in particular $\hull{e_1, \dots, e_i} = \hull{v_1, \dots, v_i}$. This follows by computation.
    \item $\omega(e_i)(e_i) \neq 0$, which is equivalent to $e_i \neq 0$ (due to the nondegeneracy condition on the form). This follows from linear independence of the $v_i$.
  \end{enumerate}
\end{proof}

Note that to do normalisation as well, we would have to take roots in the field over which the vector space lives. This does work over e.g. the real or complex numbers obviously.\\

The Gram-Schmidt procedure can be interpreted as splitting the space into a one dimensional subspace and its orthogonal complement, then passing to that complement to continue inductively. From this viewpoint, the nondegeneracy enters when we try to split the space into the one dimensional subspace and its orthogonal complement.

Now we would like to loosen this requirement somewhat.
As we have seen in Lemma \ref{lem:characterisation of nowhere degeneracy}, (weak) degeneracy on one dimensional subspaces actually implies it on all subspaces. This is not surprising as the nondegeneracy condition becomes harder to fulfill, the smaller the subspaces.

So we may hope to get something useful from considering higher dimensional subspaces, and actually dimension 2 turns out to be sufficient.

\begin{lem}[Symplectic Basis]\label{lem:symplectic basis}
  Remark: This is a purely classical lemma (at least the proof given), I don't know if that can be fixed in some generality.\\

  Let $\omega$ be a nondegenerate form with symmetric kernel, then we may decompose the space $V$ into orthogonal subspaces $L_1, \dots, L_k, P_1, \dots, P_n$, with $\dim{L_j} = 1$ and $\dim{P_j} = 2$, such that the induced map $L_1 \oplus \dots \oplus L_k \oplus P_1 \oplus \dots \oplus P_n \rightarrow V$ becomes an isomorphism.
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:symplectic basis}]
  If there are no $x \in V$ with $\omega(x)(x) \neq 0$, then the form is alternating. In this case, choose any line $L = \hull{l}$ in $V$ and by nondegeneracy of the form we may pick a line $L' = \hull{l'}$ with $\omega(l)(l') \neq 0$. Set $P_1 \coloneqq \hull{l,l'}$ and observe that as the form is alternating, the lines intersect transversally, so $P_1$ will be of dimension 2. Further given $v \in P_1$, $v = a + b \neq 0$ for some $a \in L$ and $b \in L'$ and without loss of generality say $b \neq 0$, we have $\omega(a+b)(b) = \omega(a)(b) \neq 0$. So the form becomes nondegenerate on $P_1$ and we may split the space as $P_1 \oplus \orth{P_1}$ and inductively pass to the orthogonal complement of $P_1$.

  If there is some $x \in V$ with $\omega(x)(x) \neq 0$, then set $L_1 \coloneqq \hull{x}$ and the space splits as $L_1 \oplus \orth{L_1}$. Inductively pass to the orthogonal complement of $L_1$.
\end{proof}

Note that in the case where the form is antisymmetric, we obtain an ordinary symplectic basis of the space, and in the case where the form is nondegenerate everywhere, we obtain an orthogonal basis of the space.\\

Finally we would like to show a particular construction which allows to pin down the nondegenenerate part of a form on a given subspace $U$.

\begin{lem}[Enforcing nondegeneracy]\label{lem:enforcing nondegeneracy}
  Given a form with symmetric kernel and a subspace $U \inj{} V$ we claim that it descends to a dondegenerate one on $\faktor{U}{U \cap \orth{U}}$.
\end{lem}

\begin{proof}[Proof of Lemma \ref{lem:enforcing nondegeneracy}]
  Let $\omega$ have symmetric kernel, and restrict it to $U$ to obtain $\map{\pullback{i}\omega}{U}{\dual{U}}$. It clearly descends to the quotient by its kernel $\ker{\pullback{i}\omega} = U \cap \orth{U}$, yielding an injection and retaining its image. What remains to be seen, is that the image of $\pullback{i}\omega$ consists of funtions which vanish on this kernel. Now given $\omega(u)$ for some $u \in U$, we find that for all $y \in U \cap \orth{U}$ as $y \in \orth{U}$ $\omega(y)(u) = 0$. But due to symmetry, this implies $\forall y \in U \cap \orth{U}.~ \omega(u)(y) = 0$.
\end{proof}
