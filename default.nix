{ pkgs ? import <nixpkgs> {}, ref ? "master", rev ? null, useCwd ? false }:
let
  src = if useCwd then ./. else (
    fetchGit ( {
        url = ./.;
        inherit ref;
      } // (
        if rev != null then { inherit rev; } else {}
        )
      )
    );
  tex = pkgs.texlive.withPackages (ps: [
    ps.scheme-basic
    ps.collection-luatex
    ps.fontspec
    ] ++ texPackages ps );
  texPackages = ps: with ps; [
    latexmk
    xcolor
    scalerel
    mathtools etoolbox
    faktor
    ];
in pkgs.runCommand "xaverdh-math-notes" {
     buildInputs = [ tex ];
   } ''
    mkdir $out
    export TEXMFVAR="$NIX_BUILD_TOP"
    cd ${src}
    latexmk -lualatex -output-directory="$NIX_BUILD_TOP" -aux-directory="$NIX_BUILD_TOP" -jobname=result main.tex
    cp "$NIX_BUILD_TOP"/result.pdf $out/math-notes.pdf
  ''
