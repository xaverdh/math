\section{Curvature of Curves}

\newcommand{\inner}[2]{\langle #1, #2 \rangle}

Consider a smooth curve $\map{\gamma}{I}{R^n}$ (on an interval $I \subset R$) and its jet $\gamma^{(1)}, \dots, \gamma^{(j)}$.

\subsection{The Frenet Frame}

Assuming that the jet of $\gamma$ is linearly independent (we will call such curves generic), we may orthonormalize it to obtain recursively what is called its Frenet frame:

\begin{align*}
  & v_j \defeq \gamma^{(j)} - \sum_{i=1}^{j-1} \inner{\gamma^{(j)}}{e_i}\cdot e_i \\
  & e_j \defeq v_j / |v_j|
\end{align*}

We claim that $e_i^{(1)} \subset \operatorname{span}(e_{i-1},e_{i+1})$ (we define $e_0$ and $e_{n+1}$ to vanish).

\begin{proof}
  By direct computation see that $e_1^{(1)}$ lies in the subspace spanned by $e_2$. Now proceed inductively, say the statement holds for all $i < j$, then we prove it for $j$.

  Denote $E_k \defeq \operatorname{span}(e_1,\dots,e_k)$, then by construction $\gamma^{(m)} \in E_m$ for all $m$ and by induction hypothesis $e_i^{(1)} \in E_{i+1}$ (for $i < j$).\\

  For $i \geq j + 2$, we then have $\gamma^{(j+1)} \in E_{j+1}$ as well as for all $m \leq j-1$ that both $e_m$ and $e_m^{(1)}$ are in $E_j$, so $v_j^{(1)}$ (which involves only such terms) and therefore also $e_j^{(1)}$ lie in $E_{j+1}$.\\

  Also by a direct computation:
  \begin{align*}
    & e_j^{(1)} = \frac{1}{|v_j|} \cdot ( v_j^{(1)} - e_j \cdot \inner{ v_j^{(1)} }{ e_j } )
  \end{align*}
  So clearly we have $e_j^{(1)} \perp e_j$.\\

  Futher for $i \leq j - 1$ since $e_j \perp e_i$, we compute $\inner{ e_j^{(1)} }{ e_i } = - \inner{e_j}{ e_i^{(1)} }$, and by inductive hypothesis $\inner{e_j}{ e_i^{(1)} }$ vanishes for all $i \leq j - 2$. All together his shows $e_j^{(1)} \in \operatorname{span}(e_{j-1},e_{j+1})$.

  But the case $i = j -1$ yields $\inner{ e_j^{(1)} }{ e_{j-1} } = - \inner{e_j}{ e_{j-1}^{(1)} }$, which is also of interest, so we remember it for later.
\end{proof}

After establishing this fact, we may now write $e_j^{(1)} = \lambda_j \cdot e_{j-1} + \rho_{j} \cdot e_{j+1}$ and would like to further analyze these coefficients.

First of all trivially $\lambda_j = \inner{ e_j^{(1)} }{ e_{j-1} }$.

And from the proof above we can extract the following:
\begin{align*}
  \lambda_{j} = \inner{ e_j^{(1)} }{ e_{j-1} } &= - \inner{e_j}{ e_{j-1}^{(1)} } \\
  &= - \inner{e_j}{ \lambda_{j-1} \cdot e_{j-2} + \rho_{j-1} \cdot e_j } \\
  &= - \rho_{j-1}
\end{align*}

So we arrive at the Frenet–Serret formulas:
\begin{align*}
  e_j^{(1)} &= \lambda_j \cdot e_{j-1} + - \lambda_{j+1} \cdot e_{j+1} \\
  &= ( - \chi_{j-1} \cdot e_{j-1} + \chi_{j} \cdot e_{j+1} ) \cdot |\gamma^{(1)}|
\end{align*}

where we introduced the (perhaps) more familiar notation

\begin{align*}
  \chi_j &= \frac{ - \lambda_{j+1} }{ |\gamma^{(1)}| }
  = \frac{ \rho_j }{ |\gamma^{(1)}| }
  = \frac{ \inner{ e_j^{(1)} }{ e_{j+1} } }{ |\gamma^{(1)}| }
\end{align*}

These $\chi_1, \dots, \chi_{n-1}$ are called the the (generalized) curvatures of the curve $\gamma$. Their reason d'être, as we will see in the next section, is that these are invariant under reparametrisation of the curve.

\subsection{Reparametrisation Invariance of the Frenet Frame}

Let $\map{\phi}{I}{J}$ be an orientation preserving diffeomorphism of intervals, and $\map{\gamma}{J}{R^n}$ a curve with linearly independent jet, and define $\map{\overline{\gamma}}{I}{R^n}$, $\overline{\gamma} \defeq \gamma \circ \phi$ to be its reparametrisation. We would like to express the Frenet Frame $\overline{e}_j$ of $\overline{\gamma}$ in terms of the Frenet Frame $e_j$ of $\gamma$. \\

We claim that $\overline{e}_j = e_j \circ \gamma$ and proceed by induction:

\begin{proof}
  Let $\overline{e}_i = e_i \circ \gamma$ for all $i < j$, we show that the same holds for $j$.\\

  First of all note that by the induction hypothesis subspaces spanned by the $\overline{e}_i$ and by the $e_i$ (for $i < j$) coincide.

  This also shows that the $\overline{e}_i$ for $i < j$ are still linearly independent, and the frame of $\overline{\gamma}$ is well defined at all. \\

  So we have $E_{j-1} \defeq \operatorname{span}( e_1, \dots, e_{j-1} ) = \operatorname{span}( \overline{e}_1, \dots, \overline{e}_{j-1} )$ and denote orthogonal projection onto this subspace by $\operatorname{pr}_{E_{j-1}}$.

  By computing derivatives we see that $\overline{\gamma}^{(j)} = (\gamma^{(j)} \circ \phi) \cdot (\phi^{(1)})^j + R$

  with $R \in \operatorname{span}( \gamma^{(1)} \circ \phi, \dots, \gamma^{(j-1)} \circ \phi)$ pointwise. And since $R$ is left untouched by the projection, the claim follows:

  \begin{align*}
    \overline{\gamma}^{(j)} - \operatorname{pr}_{E_{j-1}}( \overline{\gamma}^{(j)} )
    &= (\gamma^{(j)} \circ \phi) \cdot (\phi^{(1)})^j - \operatorname{pr}_{E_{j-1}}( \gamma^{(j)}) + R - \operatorname{pr}_{E_{j-1}}(R) \\
    &= (\gamma^{(j)} \circ \phi) \cdot (\phi^{(1)})^j + 0\\
    \overline{e}_j &= e_j \circ \phi
  \end{align*}

\end{proof}

While the frame itself is invariant, as we have just seen, its derivative is not. But it transforms simply by a factor of $\phi^{(1)} = |\phi^{(1)}|$, which equivalently means that the curvatures $\chi_j$ \emph{are} invariant:

\begin{align*}
  \overline{\chi}_j
  &= \frac{ \inner{ \overline{e}_j^{(1)} }{ \overline{e}_{j+1} } }{ |\overline{\gamma}^{(1)}| }
  = \frac{ \inner{ (e_j^{(1)} \circ \phi) \cdot \phi^{(1)} }{e_{j+1} \circ \phi } }{ |(\gamma^{(1)} \circ \phi) \cdot \phi^{(1)}|}
  = \chi_j \circ \phi
\end{align*}

Here we also see the behaviour under orientation reversing diffeomorphisms $\phi$. Then $|\phi^{(1)}| = - \phi^{(1)}$, and $\overline{e}_m = (e_m \circ \phi) \cdot (-1)^{m}$, so we compute the following:

\begin{align*}
  \overline{\chi}_j
  &= \frac{ \inner{ (-1)^{j} \cdot (e_j \circ \phi)^{(1)} }{ (-1)^{j+1} \cdot (e_{j+1} \circ \phi)} }{ |\overline{\gamma}^{(1)}| } \\
  &= (-1)^{j+j+1} \cdot \frac{ \phi^{(1)} }{ |\phi^{(1)}| } \cdot (\chi_j \circ \phi) = \chi_j \circ \phi
\end{align*}

So while the frame changes under orientation reversal, the curvatures do not.

\subsection{Expressions for the Curvatures}

Next we would like to take a closer look at the curvatures.
Denote by $N_j \defeq |v_j|$ the normalisation factors in the Gram-Schmidt process, then we claim that:
\begin{align*}
  \chi_j = \frac{ N_{j+1} }{ N_j } \cdot \frac{ 1 }{ |\gamma^{(1)}| }
\end{align*}

\begin{proof}
  We remind ourselves that $e^{(1)}_j = \frac{1}{N_j} \cdot (v_j^{(1)} - e_j \cdot \inner{ v_j^{(1)} }{ e_j })$, and therefore $\inner{ e^{(1)}_j }{ e_{j+1} } = \frac{1}{N_j} \inner{ v_j^{(1)} }{e_{j+1}}$.

  Now we compute $v_j^{(1)}$:

  \begin{align*}
    v_j^{(1)} &= \gamma^{(j+1)} - \sum_{i=1}^{j-1} \inner{ \gamma^{(j+1)} }{ e_i } \cdot e_i + R_1 \\
    &= v_{j+1} + R_2
  \end{align*}

  With $R_1 \in E_{j} = \operatorname{span}(e_1,\dots,e_j)$ and therefore $R_2 = R_1 + \inner{ \gamma^{(j+1)} }{ e_{j} } e_{j}$ as well, we see that $\inner{ v_j^{(1)} }{ e_{j+1} } = \inner{ v_{j+1} }{ e_{j+1} } = N_{j+1}$.
\end{proof}

From this we immediately see that the curvatures are always strictly positive.


\subsection{Orientation of the Frenet Frame}

Since the Gram-Schmidt procedure preserves orientation, the Frenet frame inherits its orientation from the original jet of the curve $\gamma$.

But notice that the Frenet formulas remain invariant under flipping the sign of both the top curvature $\chi_{n-1}$ (or equivalently $\lambda_n$) and of the last vector in the frame $e_{n}$ (this is only true for the first and the top curvatures / vectors respectively).

Assuming a fixed orientation of the ambient space, this peculiarity allows to define a new frame, otherwise identical to the Frenet one, by choosing the sign of the last vector such that it have positive orientation. This new frame also obeys the Frenet-Serret formulas if we adjust the sign of $\chi_{n-1}$ accordingly. The so adjusted top curvature is then often called the \emph{signed curvature} or \emph{torsion} of the curve, and may now take negative values.




\subsection{Reconstructing the Curve from its Curvatures}

A natural question that now arises, is whether the original curve can be reconstructed from its curvatures.

The first step to answer this question, is to look at the uniqueness proproperties of the Frenet-Serret equations:

Since the $\lambda_j$ are bounded (due to compactness of the interval, on which they live), and the Frenet-Serret equations are linear in the Frenet frame, we immediately have existance and uniqueness by the Picard–Lindelöf theorem, given an initial value for $e_j(0), j \in 1,\dots, n$ (we assume $0 \in I$, $\map{\gamma}{I}{R^n}$). But we need to assume that the curve $\gamma$ be parametrised by arc length (such that the $\lambda_j$ are fixed by the curvatures).

Such an initial value then corresponds to the choice of an element in the orthogonal group $O(n)$.
And due to the uniqueness of the solution, such a choice will also fully determine the entire Frenet frame.

If we are given in addition an initial point in $p \in R^n$ s.t. $\gamma(0) = p$, then by integrating $e_1(t)$, we may reconstruct the original curve up to reparametrisation (the reconstruction will always be parametrised by arc length).

So in summary we find a one to one correspondance:

\begin{samepage}

\begin{center}
  [Generic (smooth) curves on $I$ (with marked point 0) up to orientation preserving reparametrisation]

  \nopagebreak

  \leftrightarrow

  \nopagebreak

  [Tuples $(p \in R^n, e \in O(n),\map{\chi_1,\dots,\chi_{n-1}}{ I }{ R_{>0} }$)]
\end{center}

\end{samepage}
Note that if one takes the convention of flipping signs in the top curvature and its corresponding vector $e_n$, then this correspondance will look slightly different. Instead of choosing an element in $O(n)$, one is restricted to $SO(n)$, and the top curvature may take negative values instead (it carries the orientation bit). The analogous statement holds when flipping the sign of $e_1$ and $\chi_1$ accordingly.
